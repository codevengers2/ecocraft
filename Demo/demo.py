import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QPushButton,QStackedWidget


class Window1(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Window 1")
        layout = QVBoxLayout()
        self.button = QPushButton("Show Window 2")
        layout.addWidget(self.button)
        self.setLayout(layout)
        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)

        self.main_window_widget = QWidget()
        self.init_main_window()
        self.stacked_widget.addWidget(self.main_window_widget)
        
        self.description = ""
        

    def show_main_window(self):
        self.stacked_widget.setCurrentWidget(self.main_window_widget)
        
    def open_second_window(self):
        self.second_window = Window2()
        self.second_window.action_button.clicked.connect(self.show_main_window)
        self.stacked_widget.addWidget(self.second_window)
        self.stacked_widget.setCurrentWidget(self.second_window)



class Window2(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Window 2")
        layout = QVBoxLayout()
        button = QPushButton("Button 2")
        layout.addWidget(button)
        self.setLayout(layout)


class ParentWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Parent Window")
        
        # Create instances of Window1 and Window2
        self.window1 = Window1()
        self.window2 = Window2()
        
        # Create a central widget and set the layout
        central_widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.window1)
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        
        # Connect button click signal to show Window2
        self.window1.button.clicked.connect(self.show_window2)
    
    def show_window2(self):
        self.window1.hide()
        self.window2.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = ParentWindow()
    main_window.show()
    sys.exit(app.exec_())


















# import sys
# import firebase_admin
# from firebase_admin import credentials, storage
# from PyQt5.QtWidgets import (
#     QApplication,
#     QWidget,
#     QMainWindow,
#     QHBoxLayout,
#     QLabel,
#     QPushButton,
#     QVBoxLayout,
#     QDesktopWidget,
#     QGroupBox,
#     QListWidget,
#     QListWidgetItem,
#     QSplitter,
#     QScrollArea,
#     QGridLayout,
# )
# from PyQt5.QtGui import QPixmap
# from PyQt5.QtCore import Qt
# # from addCart import CartView, AddToCartUI, MainCartApp

# class MainWindow(QMainWindow):
#     def __init__(self):
#         super().__init__()
#         self.setWindowTitle("Product Purchase")
#         self.setObjectName("Widget")
#         self.setStyleSheet("background-color: white;")

#         self.init_ui()

#     def init_ui(self):
#         # Set up the app bar
#         self.setup_appbar()

#         # Set up the main content
#         # self.setup_content()

#     def setup_appbar(self):
#         # Create a central widget
#         central_widget = QWidget()
#         self.setMenuWidget(central_widget)

#         # Create a layout for the app bar
#         appbar_layout = QHBoxLayout()

#         # Add additional buttons or widgets as needed
#         # For example, a button for some action
#         self.action_button = QPushButton("Back", self)
#         self.action_button.setFixedSize(100, 30)  # Set width and height
#         self.action_button.setStyleSheet(
#             "background-color: #7D7C7C; color: white; padding: 10px 20px; border: none; border-radius: 5px;"
#         )
#         appbar_layout.addWidget(self.action_button)

#         # Create a label for the app bar title
#         appbar_title = QLabel("EcoCraft", self)
#         appbar_title.setStyleSheet("font-size: 16px;")
#         appbar_title.setAlignment(Qt.AlignCenter)
#         appbar_layout.addWidget(appbar_title)
#         # Set the background color of the app bar
#         appbar_widget = QWidget()
#         appbar_widget.setStyleSheet(
#             "background-color: #F1EFEF; color: Black; border: none; border-radius: 5px;"
#         )  # Set background color to black
#         appbar_widget.setLayout(appbar_layout)

#         layout = QHBoxLayout(central_widget)
#         layout.addWidget(appbar_widget)
        
# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#     window = MainWindow()
#     window.showMaximizedMaximized()
#     sys.exit(app.exec_())
