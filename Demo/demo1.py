import tkinter as tk
from tkinter import ttk, messagebox
from PIL import Image, ImageTk
import firebase_admin
from firebase_admin import credentials, firestore, auth, initialize_app
# from login import Login

# Initialize Firestore
cred = credentials.Certificate("D:/Python/X/setup/super-xdemo-firebase-adminsdk-yjtfo-8114a3be5e.json")
firebase_admin.initialize_app(cred)
db = firestore.client()

class SignupWindow(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Signup")
        self.state('zoomed')
        self.parent = parent  # Store the parent reference
        # Add your signup interface widgets here
        self.create_main_frame()
        self.create_entry_fields()
        self.create_signup_button()
        
    def create_main_frame(self):
        self.main_frame = tk.Frame(self)
        self.main_frame.configure(bg="green")
        self.main_frame.pack(fill=tk.X, pady=1)

    def create_entry_fields(self):
        labels = ["Name", "Email", "Phone", "Username", "Password"]
        entries = [tk.Entry(self.main_frame) for _ in range(len(labels))]
        for i, label in enumerate(labels):
            tk.Label(self.main_frame, text=label + ":", font=("Arial", 14)).grid(row=i, column=0, padx=10, pady=5, ipadx=55)
            entries[i].grid(row=i, column=1)
        self.name_entry, self.email_entry, self.phone_entry, self.username_entry, self.password_entry = entries

    def create_signup_button(self):
        signup_button = tk.Button(self.main_frame, text="Signup", command=self.signup)
        signup_button.grid(row=5, column=0, columnspan=2)

    def signup(self):
        name = self.name_entry.get()
        email = self.email_entry.get()
        phone = self.phone_entry.get()
        username = self.username_entry.get()
        password = self.password_entry.get()

        doc_ref = db.collection('users').document(username)  # Use db here instead of self.db
        doc_ref.set({
            'name': name,
            'email': email,
            'phone': phone,
            'password': password,
            'first_time': True  # Add 'first_time' attribute
        })

        print("User signed up successfully!")


class WelcomeWindow(tk.Toplevel):
    def __init__(self, parent, welcome_message):
        super().__init__(parent)
        self.title("Welcome")
        self.state('zoomed')

        # Display frame
        user_profile_frame = tk.Frame(self)
        user_profile_frame.pack(padx=10, pady=10)
        user_profile_frame.configure(bg="#192F44")

        name_label1 = tk.Label(user_profile_frame, text=welcome_message, background="#192F44", fg="white", font=('Calibri', 40))
        name_label1.pack(pady=10, ipadx=550, ipady=10)

        # Input Frame
        input_frame = tk.Frame(self)
        input_frame.pack(pady=10)

        # Label for driving license
        driving_license_label = tk.Label(input_frame, text="Do you have Driving Licence?", background="#192F44", fg="white", font=('Calibri', 20))
        driving_license_label.grid(row=0, column=0, padx=10, pady=5)

        # Radio buttons for Yes and No
        driving_license_var = tk.StringVar(value="No")
        tk.Radiobutton(input_frame, text="Yes", variable=driving_license_var, value="Yes", fg="white", font=('Calibri', 16)).grid(row=0, column=1, padx=10, pady=5)
        tk.Radiobutton(input_frame, text="No", variable=driving_license_var, value="No", fg="white", font=('Calibri', 16)).grid(row=0, column=2, padx=10, pady=5)


class LoginWindow(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Login")
        self.state('zoomed')
        # Add your login interface widgets here
        self.create_main_frame()
        self.create_entry_fields()
        self.create_login_button()
    
    def create_main_frame(self):
        self.main_frame = tk.Frame(self)  # Use self instead of self.root
        self.main_frame.configure(bg="green")
        self.main_frame.pack(fill=tk.X, pady=1)

    def create_entry_fields(self):
        tk.Label(self.main_frame, text="Username:", font=("Arial", 14)).grid(row=0, column=0, padx=10, pady=5, ipadx=55)
        self.username_entry = tk.Entry(self.main_frame)
        self.username_entry.grid(row=0, column=1)
        tk.Label(self.main_frame, text="Password:", font=("Arial", 14)).grid(row=1, column=0, padx=10, pady=5, ipadx=55)
        self.password_entry = tk.Entry(self.main_frame, show='*')
        self.password_entry.grid(row=1, column=1)

    def create_login_button(self):
        login_button = tk.Button(self.main_frame, text="Login", command=self.login)
        login_button.grid(row=2, column=0, columnspan=2)

    def login(self):
        username = self.username_entry.get()
        password = self.password_entry.get()

        doc_ref = db.collection('users').document(username)
        doc = doc_ref.get()
        
        if doc.exists:
            user_data = doc.to_dict()
            if user_data['password'] == password:
                welcome_message = ""
                if 'first_time' in user_data:
                    welcome_message = "You are visiting our page for the first time."
                    # Update the user's data in the Firestore database to remove the 'first_time' field
                    doc_ref.update({'first_time': firestore.DELETE_FIELD})
                else:
                    welcome_message = "Welcome back, " + user_data['name']

                WelcomeWindow(self, welcome_message)
            else:
                print("Invalid username or password!")
        else:
            print("User does not exist!")


class RentWheels:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title("")
        self.root.state('zoomed')
        self.canvas = tk.Canvas(self.root)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.canvas.configure(bg='White')
        # Add a Scrollbar for vertical scrolling
        self.scrollbar = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command=self.canvas.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        # Configure canvas
        self.canvas.configure(yscrollcommand=self.scrollbar.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox('all')))
        self.canvas.bind_all("<MouseWheel>", lambda event: self.canvas.yview_scroll(int(-1*(event.delta/120)), "units"))
        self.addmainframe()
        self.addupperframe()
        self.showupperframe()
        self.root.mainloop()
    
    def addmainframe(self):
        # Create a frame to hold the entire content
        self.main_frame = tk.Frame(self.canvas)
        self.main_frame.configure(bg="White")
        self.canvas.create_window((0, 0), window=self.main_frame, anchor='nw')

    def addupperframe(self):
        # upper frame
        self.upper_frame = tk.Frame(self.canvas)
        self.upper_frame.pack(fill=tk.X, pady=1)
        self.upper_frame.configure(bg="#000")

    def showupperframe(self):  
        # Load image
        self.original_image_pil = Image.open("C:/Users/prana/Downloads/rentwheels.png")
        self.resized_image_pil = self.original_image_pil.resize((500, 134))
        self.resized_image_tk = ImageTk.PhotoImage(self.resized_image_pil)

        # Add logo label
        self.logo_label = tk.Label(self.upper_frame, image=self.resized_image_tk, borderwidth=0, relief=tk.FLAT)
        self.logo_label.grid(row=0, column=0, sticky=tk.W, columnspan=6, padx=10)

        # Add privacy policy button
        self.privacy_label = tk.Button(self.upper_frame, text="Privacy Policy", background='Black', fg="White", borderwidth=0,font=('Poppins', 14))
        self.privacy_label.place(x=self.root.winfo_screenwidth() - 525, y=50)

        # Add wishlist button
        self.login_label = tk.Button(self.upper_frame, text="Login", background='Black', fg="White", borderwidth=0,font=('Poppins', 14), command=self.open_login_window)
        self.login_label.place(x=self.root.winfo_screenwidth() - 280, y=50)

        # Add login/signup button
        self.signup_label = tk.Button(self.upper_frame, text="Signup", background='Black', fg="White", borderwidth=0,font=('Poppins', 14),command=self.open_signup_window)
        self.signup_label.place(x=self.root.winfo_screenwidth() - 150, y=50)

    def open_login_window(self):
        LoginWindow(self.root)

    def open_signup_window(self):
        SignupWindow(self.root)

if __name__ == "__main__":
    app = RentWheels()
